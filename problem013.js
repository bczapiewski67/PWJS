function parametricSort(a, b){

  function compareNumbersa(a,b){
    return a-b;
  }
  
  function compareNumbersb(a,b){
    return b-a;
  }
  
  if(b!="asc" && b!="desc")
    return false;
  
  else if(b=="asc")
    a.sort(compareNumbersa);
  
  else if(b=="desc")
    a.sort(compareNumbersb);
    
  return a;
}
