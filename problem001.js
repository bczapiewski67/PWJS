function triangleArea(a, b, c){
  var outcome;
  if(a<=0 || b<=0 || c<=0 || a+b<c || b+c<a || b+c<a)
    return -1;
  
  else{
    var p = (a+b+c)/2;
    outcome = Math.sqrt(p*(p-a)*(p-b)*(p-c));
    outcome=Math.round(outcome*=100)/100;
    
        
    return outcome;
  }
}