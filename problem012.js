function objectsDiff(a, b) {
var tab = [];
  
  for(var i in a){
    var is = true;
    for(var j in b){
      if(i==j)
        is = false;
    }
    if(is)
      tab.push(i);
  }
  
  for(var i in b){
    var is = true;
    for(var j in a){
      if(i==j)
        is = false;
    }
    if(is)
      tab.push(i);
  }
  
  return tab;
}