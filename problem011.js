function deepFindAndCount(a, b) {
  var counter = 0;
  for(var i in a){
    if(Array.isArray(a[i])){
      var q = deepFindAndCount(a[i],b);
      counter+=q;
    }
    else{
      if(a[i]==b)
        counter++;
    }
  }
  
  return counter;
}