function strangeSum(a, b){
  if((typeof a != "number") || (typeof b != "number") || (Math.round(a)!=a) || (Math.round(b)!=b))
    return null;
  
  else if(a==b)
    return (a+b)*3;
  
  else if(a!=b)
    return a+b;
}